<?php

include 'vendor/autoload.php';

use chillerlan\QRCode\{QRCode, QROptions};
use chillerlan\QRCode\Output\{QRCodeOutputException, QRImage};

class QRImageWithLogo extends QRImage
{

  /**
   * @param string|null $file
   * @param string|null $logo
   *
   * @return string
   * @throws \chillerlan\QRCode\Output\QRCodeOutputException
   */
  public function dump(string $file = null, string $logo = null): string
  {
    // set returnResource to true to skip further processing for now
    $this->options->returnResource = true;

    // of course you could accept other formats too (such as resource or Imagick)
    // i'm not checking for the file type either for simplicity reasons (assuming PNG)
    if (!is_file($logo) || !is_readable($logo)) {
      throw new QRCodeOutputException('invalid logo');
    }

    $this->matrix->setLogoSpace(
      $this->options->logoSpaceWidth,
      $this->options->logoSpaceHeight
      // not utilizing the position here
    );

    // there's no need to save the result of dump() into $this->image here
    parent::dump($file);

    $im = imagecreatefrompng($logo);

    // get logo image size
    $w = imagesx($im);
    $h = imagesy($im);

    // set new logo size, leave a border of 1 module (no proportional resize/centering)
    $lw = ($this->options->logoSpaceWidth - 2) * $this->options->scale;
    $lh = ($this->options->logoSpaceHeight - 2) * $this->options->scale;

    // get the qrcode size
    $ql = $this->matrix->size() * $this->options->scale;

    // scale the logo and copy it over. done!
    imagecopyresampled($this->image, $im, ($ql - $lw) / 2, ($ql - $lh) / 2, 0, 0, $lw, $lh, $w, $h);

    $imageData = $this->dumpImage();

    if ($file !== null) {
      $this->saveToFile($imageData, $file);
    }

    if ($this->options->imageBase64) {
      $imageData = 'data:image/' . $this->options->outputType . ';base64,' . base64_encode($imageData);
    }

    return $imageData;
  }
}

class LogoOptions extends QROptions
{
  // size in QR modules, multiply with QROptions::$scale for pixel size
  protected int $logoSpaceWidth;
  protected int $logoSpaceHeight;
}

$options = new LogoOptions;

$options->versionMin          = 19;
$options->versionMax          = 40;
$options->eccLevel         = QRCode::ECC_H;
$options->imageBase64      = true;
$options->logoSpaceWidth   = 35;
$options->logoSpaceHeight  = 40;
$options->scale            = 3;
$options->imageTransparent = false;

$teste = null;

$erro = null;

if (isset($_POST['qrcode'])) {

  $dados = $_POST["qrcode"];

  try {
    $qrOutputInterface = new QRImageWithLogo($options, (new QRCode($options))->getMatrix("$dados"));

    $teste = $qrOutputInterface->dump(null, 'ciacommec.png');
  } catch (Exception $e) {
    $erro = "Excedeu o número de caracteres permitidos. Exceção capturada: " .  $e->getMessage();
  }
}

$file = "contador.txt";
// Arquivo texto para manter o valor da variável

if (file_exists($file)) {

  $handle = fopen($file, 'r+');
  // Definimos o arquivo, as permissões para ler e escrever, por isso o pârametro r+ (ler e escrever)

  $data   = fread($handle, 512);
  // obtém o valor que está no arquivo contador.txt

  if(is_numeric($data)) {
    $contar = $data + 1;
  } else {
    $contar = 1;
  }
  // Adiciona +1

  // Exibe na tela o valor encontrado no arquivo TXT

  fseek($handle, 0);
  // O ponteiro volta para o início do arquivo

  fwrite($handle, $contar);
  // Salva o valor da variável contar no arquivo

  fclose($handle);
  // Fecha o arquivo
} else {

  $create = fopen("contador.txt", "w");

  $contar = 1;

  fseek($create, 0);
  // O ponteiro volta para o início do arquivo

  fwrite($create, $contar);
  // Salva o valor da variável contar no arquivo

  fclose($create);

}

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <TITLE>QR Code</TITLE>
  <meta charset="UTF-8" />
  <meta lang="pt-BR" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Gerador de QR Code" />
  <meta name="author" content="1º Sgt Ferreira" />
  <link rel="icon" href="/ciacommec.png" />
  <link href="/css/bootstrap2.min.css" rel="stylesheet" type="text/css" />
  <script src="/js/jquery2.min.js" type="text/javascript"></script>
  <link href="/node_modules/toastr/build/toastr.css" rel="stylesheet" />
  <script src="/node_modules/toastr/build/toastr.min.js"></script>
  <script>
    toastr.options = {
      closeButton: false,
      debug: false,
      newestOnTop: true,
      progressBar: true,
      positionClass: "toast-top-right",
      preventDuplicates: false,
      onclick: null,
      showDuration: "300",
      hideDuration: "1000",
      timeOut: "5000",
      extendedTimeOut: "1000",
      showEasing: "swing",
      hideEasing: "linear",
      showMethod: "fadeIn",
      hideMethod: "fadeOut",
    };
  </script>
  <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#4169E1" />
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#4169E1" />
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
</head>

<body>
  <div class="container">
    <div class="jumbotron">
      <h1 style="text-align: center;">Gerador de QR Code</h1>
      <?php if ($teste !== null) : ?>
        <a href="<?php echo $teste ?>" download="QRCode">
          <img class="img-responsive center-block" src="<?php echo $teste ?>" />
        </a>
        <p class="text-center">
          <a class="btn btn-primary" href="<?php echo $teste ?>" download="QRCode">Download</a>
          <button class="btn btn-info" onclick="printImg('<?php echo $teste ?>')">Imprimir</button>
        </p>
      <?php endif; ?>
      <?php if ($erro !== null) : ?>
        <script>
          toastr.error(`<?php echo $erro ?>`)
        </script>
      <?php endif; ?>
    </div>
    <form method="POST" action="">
      <label id="count">Digite o texto:</label>
      <textarea class="form-control" rows="10" id="qrcode" name="qrcode" placeholder="Texto do QR Code" required='required' maxlength="1273"></textarea>
      <button class="btn center-block" type="submit" id="button">Gerar QRCode</button>
    </form>
  </div>
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>Desenvolvido pelo 1º Sgt Ferreira.</p>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(function() {
      $('#qrcode').keyup(function() {
        var x = $('#qrcode').val();

        var newLines = x.match(/(\r\n|\n|\r)/g);
        var addition = 0;
        if (newLines != null) {
          addition = newLines.length;
        }

        chars = 1273 - (x.length + addition)

        if (chars == 1273) {
          $('#button').attr("disabled", "disabled").addClass('btn-danger').removeClass('btn-success');
          $('#qrcode').css("border-color", "red");
        }

        if (chars < 0) {
          $('#button').attr("disabled", "disabled").addClass('btn-danger').removeClass('btn-success');
          $('#qrcode').css("border-color", "red");
        }

        if (chars >= 1 && chars < 1273) {
          $('#button').removeAttr('disabled').removeClass('btn-danger').addClass('btn-success');
          $('#qrcode').css('border-color', 'green');
        }

        $('#count').html("Caracteres restantes: " + chars);
      })
    })

    function printImg(url) {
      var win = window.open('');
      win.document.write('<img src="' + url + '" onload="window.print();" />');
      win.focus();
    }
  </script>
</body>

</html>

<?php unset($_POST['qrcode']); ?>